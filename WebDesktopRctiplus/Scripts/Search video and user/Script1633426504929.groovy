import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('rc-ugctalent.rctiplus.com')

WebUI.maximizeWindow()

WebUI.click(findTestObject('Search/Page_SEO - Ikuti Kompetisi Berhadiah Dan Tonton Video Yang Sedang Trending - HOT On RCTI - VIQI/menu_search'))

WebUI.click(findTestObject('Search/Page_Cari konten video dan user viqi terbaru dan terpopular - HOT On RCTI - VIQI/tab_user'))

WebUI.setText(findTestObject('Search/Page_Cari konten video dan user terbaru dan terpopular - HOT On RCTI - VIQI/input_search'), 
    'test')

WebUI.delay(10)

WebUI.click(findTestObject('Search/Page_Cari konten video dan user test terbaru dan terpopular - HOT On RCTI - VIQI/tbl_tabVideos'))

WebUI.delay(10)

WebUI.click(findTestObject('Search/Page_Cari konten video dan user viqi terbaru dan terpopular - HOT On RCTI - VIQI/video_play'))

WebUI.delay(10)

WebUI.click(findTestObject('Search/Page_Tonton karya video Testing upload online - HOT On RCTI - VIQI/tbl_closevideoplay'))

WebUI.closeBrowser()

