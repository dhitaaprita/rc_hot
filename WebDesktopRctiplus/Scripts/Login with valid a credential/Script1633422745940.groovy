import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

String url_rc = GlobalVariable.url_rc
String tbl_profile = 'Object Repository/Login/Page_SEO - Ikuti Kompetisi Berhadiah Dan Tonton Video Yang Sedang Trending - HOT On RCTI - VIQI/MyProfile' 
String tbl_login1 = 'Object Repository/Login/Page_SEO - Ikuti Kompetisi Berhadiah Dan Tonton Video Yang Sedang Trending - HOT On RCTI - VIQI/tbl_login'
String input_email = 'Object Repository/Login/Page_Login Akun - RCTI/email'
String input_password = 'Object Repository/Login/Page_Login Akun - RCTI/password'
String tbl_login2 = 'Object Repository/Login/Page_Login Akun - RCTI/Tombol_login2'

WebUI.openBrowser('')

WebUI.navigateToUrl(url_rc)

WebUI.maximizeWindow()

WebUI.click(findTestObject(tbl_profile))
WebUI.click(findTestObject(tbl_login1))
WebUI.setText(findTestObject(input_email), username)
WebUI.setText(findTestObject(input_password), password)
WebUI.click(findTestObject(tbl_login2))

WebUI.delay(25)

